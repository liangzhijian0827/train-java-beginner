package collection;

import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;


public class EcutSet<E> implements Set<E>{

	private transient HashMap<E,Object> map;
	private static final Object PRESENT = new Object();
	
	public EcutSet() {
        map = new HashMap<>();
    }
	
	@Override
	public boolean add(E e) {
		
		return map.put(e, PRESENT)==null;
	}

	@Override
	public boolean remove(Object o) {
		
		return map.remove(o)==PRESENT;
	}
	
	@Override
	public int size() {

		return map.size();
	}

	@Override
	public boolean isEmpty() {

		return map.isEmpty();
	}

	@Override
	public boolean contains(Object o) {
		
		return map.containsKey(o);
	}
	
	@Override
	public boolean containsAll(Collection<?> c) {

		Iterator<?> i=c.iterator();
		while(i.hasNext()) {
			if(!this.contains(i.next())) {
				return false;
			}
		}
		return true;
	}

	@Override
	public boolean addAll(Collection<? extends E> c) {
		Boolean n=false;
		Iterator<? extends E> i=c.iterator();
		while(i.hasNext()) {
			this.add(i.next());
			n=true;
		}
		return n;
	}

	@Override
	public boolean retainAll(Collection<?> c) {
		
		Iterator<?> i=this.iterator();
		while(i.hasNext()) {
			Object o=i.next();
			if(!c.contains(o)) {
				i.remove();
			}
		}
		return true;
		
	}

	@Override
	public boolean removeAll(Collection<?> c) {
		Boolean n=false;
		Iterator<?> i=c.iterator();
		while(i.hasNext()) {
			Object o=i.next();
			if(this.contains(o)) {
				this.remove(o);
				n=true;
			}
		}
		return n;
	}

	@Override
	public void clear() {
		map.clear();		
	}


	@Override
	public Iterator<E> iterator() {
		
		return map.keySet().iterator();
	}

	@Override
	public Object[] toArray() {
		Set<?> set=map.keySet();
		return set.toArray();
	}

	@Override
	public <T> T[] toArray(T[] a) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String toString() {
		return  map.keySet().toString() ;
	}

	
	// 自己借助于 HashMap 实现 java.util.Set 接口
	
}
