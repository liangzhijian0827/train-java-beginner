
public class Triangle{
	public double firstEdge;
	public double secondEdge;
	public double thirdEdge;
	
	public void squaring(){
		if(firstEdge<secondEdge+thirdEdge&&secondEdge<firstEdge+thirdEdge&&thirdEdge<firstEdge+secondEdge) {
			double p=(firstEdge+secondEdge+thirdEdge)/2;
			double s=Math.sqrt(p*(p-firstEdge)*(p-secondEdge)*(p-thirdEdge));
			System.out.println("三角形的面积为："+s);
		}
		else {
			System.out.println("输入的三边长度不对");
		}
	}
	
	public static void main(String[] args) {
		Triangle t=new Triangle();
		t.firstEdge=30;
		t.secondEdge=40;
		t.thirdEdge=50;
		t.squaring();
	}
}