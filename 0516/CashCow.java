
public class CashCow {
    private int height;
    private int goldPiece;
    private boolean wormy;
    private int pesticide;

    public void grow(){
        height=height+2;
        goldPiece=goldPiece+10;
    }
    public void buy(){
        if(goldPiece>0){
            goldPiece=goldPiece-1;
            pesticide=pesticide+100;
        }
    }
    public void kill(){
        if(wormy==true&&pesticide>=50){
            pesticide=pesticide-50;
            goldPiece=goldPiece+5;
            wormy=false;
        }
    }
    public void watering(){
        goldPiece=goldPiece+5;
        height=height+1;
        wormy=true;
    }
    public void show(){
        System.out.println("摇钱树的高度为："+height);
        System.out.println("摇钱树上的金币为："+goldPiece);
        System.out.println("摇钱树是否有虫子："+wormy);
        System.out.println("杀虫剂的数量为："+pesticide);
    }

    public static void main(String[] args) {
        CashCow cc=new CashCow();
        cc.height=0;
        cc.goldPiece=0;
        cc.wormy=false;
        cc.pesticide=0;
        cc.show();
    }
}