public class ArrayOutput {

    public static void main(String[] args) {
        char[] heavenlyStems = { '甲' , '乙' , '丙' , '丁' , '戊' , '己' , '庚' , '辛' , '壬' , '癸' };
  
        char[] earthlyBranches = { '子' , '丑' , '寅' , '卯' , '辰' , '巳' ,  '午' , '未' , '申' , '酉' , '戌' , '亥' };
        int x=0,step=0;
        for(int i=0,j=0;;i++,j++){
                System.out.print(heavenlyStems[i] +""+ earthlyBranches[((12*step)+j)%12] +"、");
                x++;
                if(x>9&&x%10==0){
                    i=i-10;
                }
                if(x>11&&x%12==0){
                    System.out.println();
                    step++;
                    j=j-12;
                }
                if(x==60)
                break;    
        }   
    }
}
