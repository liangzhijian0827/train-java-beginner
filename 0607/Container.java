package collection;

import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;

public class Container implements Collection<Object> {
	
	private static final int DEFAULT_INITIAL_CAPACITY = 1 << 4 ;
	private static final int MAXIMUM_CAPACITY = 1 << 30 ;
	private static final float DEFAULT_LOAD_FACTOR = 0.75f ;
	private static final Object UNUSED = new Object() ;
	
	private Object[] elements;
	private int counter;
	private float loadFactor;
	
	public Container(int initialCapacity,float loadFactor) {
		if(initialCapacity<0||initialCapacity>MAXIMUM_CAPACITY) {
			RuntimeException ex=new RuntimeException("初始容量必须是 [ 0 , " + MAXIMUM_CAPACITY + " ] 之间的正整数值");
			throw ex;
		}
		if(loadFactor<0||loadFactor>1) {
			RuntimeException ex=new RuntimeException("加载因子应为(0,1)之间的浮点数");
			throw ex;
		}
		this.elements=new Object[initialCapacity];
		this.loadFactor=loadFactor;
		this.mark();
	}
	
	public Container(int initialCapacity) {
		this(initialCapacity,DEFAULT_LOAD_FACTOR);
	}
	
	public Container() {
		this(DEFAULT_INITIAL_CAPACITY,DEFAULT_LOAD_FACTOR);
	}
	
	private void mark() {
		Arrays.fill(elements, UNUSED);
	}
	
	private void ensureCapacity() {
		if(counter>elements.length*loadFactor) {
			final Object[] o=this.elements; 
			int newLength=o.length*2/3+1;
			if(newLength>MAXIMUM_CAPACITY) {
				RuntimeException ex=new RuntimeException("容器的容量超过最大值");
				throw ex;
			}
			this.elements=new Object[newLength];
		}
	}
	
	private final StringBuilder builder = new StringBuilder();
	@Override
	public String toString() {
		builder.append( "[" );
		if (counter > 0) {
			for (int i = 0; i < counter; i++) {
				builder.append( elements[i] ); 
				builder.append( i < counter - 1 ? "," : "" );
			}
		}
		builder.append( "]" );
		String s = builder.toString();
		builder.setLength( 0 );
		return s;
	}

	@Override
	public int size() {
		return counter;
	}

	@Override
	public boolean isEmpty() {
		return counter==0;
	}

	@Override
	public boolean contains(Object o) {
		for(int i=0;i<counter;i++) {
			Object ob=elements[i];
			if(ob==o||(ob!=null&&ob.equals(o))) {
				return true;
			}
		}
		return false;
	}

	@Override
	public Object[] toArray() {
		Object[] array=Arrays.copyOf(elements, counter);
		return array;
	}

	@Override
	public boolean add(Object e) {
		this.ensureCapacity();
		elements[counter]=e;
		counter++;
		return true;
	}

	@Override
	public boolean remove(Object o) {
		Boolean b=this.contains(o);
		if(b==true) {
			for(int i=0;i<counter;i++) {
				Object ob=elements[i];
				if(ob==o||(ob!=null&&ob.equals(o))) {
					int n=counter-1-i;
					System.arraycopy(elements, i+1, elements, i, n);
					elements[counter-1]=UNUSED;
					counter--;
					return true;
				}
			}
		}
		return false;
	}

	@Override
	public void clear() {
		counter=0;
		this.mark();
	}
	
	@Override
	public boolean containsAll(Collection<?> c) {
		Object[] o=c.toArray();
		for(int i=0;i<o.length;i++) {
			Object n=o[i];
			if(this.contains(n)==false) {
				return false;
			}
		}
		return true;
	}

	@Override
	public boolean addAll(Collection<? extends Object> c) {
		this.ensureCapacity();
		Object[] ad=c.toArray();
		int newlength=ad.length;
		ensureCapacity();
		System.arraycopy(ad, 0, elements, counter, newlength);
		counter +=newlength;
		return true;
	}

	@Override
	public boolean removeAll(Collection<?> c) {
		Object[] o=c.toArray();
		Boolean n=false;
		for(int i=0;i<o.length;i++) {
			for(int j=0;j<elements.length;j++) {
				if(this.contains(o[i])==true) {
					this.remove(o[i]);
					n=true;
				}
				if(this.contains(o[i])==false) {
					break;
				}
			}
		}
		if(n==true) {
			return true;
		}
		return true;
	}

	@Override
	public boolean retainAll(Collection<?> c) {
		final Object[] o=elements;
		int a=0 , b=0;
		Boolean n=false;

		ensureCapacity();
		for(;a<o.length;a++) {
			if (c.contains(o[a]) == true) {

				ensureCapacity();
                elements[b++] = o[a];
                counter =b;
                n=true;
			}
		}
		if(n==true) {
			return true;
		}
		this.clear();
		return false;
	}
	
	@Override
	public Iterator<Object> iterator() { // 【不去实现】
		return null;
	}
	
	@Override
	public <T> T[] toArray(T[] a) { // 【不去实现】
		return null;
	}

}