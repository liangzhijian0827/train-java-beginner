package inherit;

public class Circle extends Shape{
	protected double radius;
	
	public Circle() {
		this.type="圆";
	}
	
	public void calculate() {
		area=Math.PI*Math.pow(radius,2);
	}
	
	public void description() {
		System.out.println("圆的半径为："+radius);
		show();
	}

	public static void main(String[] args) {
		

	}

}
