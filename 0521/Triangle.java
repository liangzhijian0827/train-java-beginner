package inherit;

public class Triangle extends Shape{
	
	protected double firstEdge;
	protected double secondEdge;
	protected double thirdEdge;
	
	public Triangle() {	
		this.type="三角形";
	}
	
	public void calculate() {
		if(firstEdge<secondEdge+thirdEdge&&secondEdge<firstEdge+thirdEdge&&thirdEdge<firstEdge+secondEdge) {
			double p=(firstEdge+secondEdge+thirdEdge)/2;
			area=Math.sqrt(p*(p-firstEdge)*(p-secondEdge)*(p-thirdEdge));

		}
		else {
			System.out.println("输入的三边长度不对");
		}
	}
	
	public void description() {
		System.out.println("三角形三边的长度为："+firstEdge+","+secondEdge+","+thirdEdge);
		show();
	}
}
