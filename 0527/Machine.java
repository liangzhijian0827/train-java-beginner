package interfacetest;

public abstract class Machine {

	private int energy;
	
	public Machine(int energy) {
		this.energy=energy;
	}
	
	public int getEnergy() {
		return energy;
	}
	
	public void setEnergy(int energy) {
		this.energy=energy;
	}
}
