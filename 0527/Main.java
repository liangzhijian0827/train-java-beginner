
package interfacetest;

import java.util.Random;

public class Main {

	public static void main(String[] args) {
		
		Random rand = new Random();

		Machine m = new Aircraft();
		boolean b=true;
		while( b ) {
		  int op = rand.nextInt( 10 ); // 随机产生 [ 0 , 10 ) 之间的数字
		  switch( op ) {
		    case 0 :
		    int m1=m.getEnergy();
		    if(m1>=10){
		    	Roadable ro=(Roadable)m;
		    	ro.run();
		    }
		    else {
		    	b=false;
		    }
		      // 需要判断能量是否足以完成 陆地驾驶 操作，若不足就跳出循环 (如何从switch直接跳出while循环?)
		      // 执行 陆地驾驶 操作 ( Machine 不具备陆地驾驶能力，需要转换为 Roadable 才可以调用 run 方法 )
		      break;
		    case 1 :
		    	int m2=m.getEnergy();
		    	if(m2>=15){
			    	Voyageable vo=(Voyageable)m;
			    	vo.voyage();
			    }
			    else {
			    	b=false;
			    }
		      // 需要判断能量是否足以完成 水中航行 操作，若不足就跳出循环
		      // 执行 水中航行 操作 ( Machine 不具备水中航行能力，需要转换为 Voyageable 才可以调用 voyage 方法 )
		      break;
		    case 2 :
		    	int m3=m.getEnergy();
		    	if(m3>=25){
		    		Flyable fl=(Flyable)m;
			    	fl.fly();
			    }
			    else {
			    	b=false;
			    }
		      // 需要判断能量是否足以完成 空中飞行 操作，若不足就跳出循环
		      // 执行 空中飞行 操作 ( Machine 不具备空中飞行能力，需要转换为 Flyable 才可以调用 fly 方法 )
		      break;
		    default :
		      // 不支持的操作
		  }
		}
		System.out.println("飞行器的剩余能量："+m.getEnergy());
	}

}
