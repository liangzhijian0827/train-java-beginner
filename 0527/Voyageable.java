package interfacetest;

public interface Voyageable {
	
	public abstract void voyage();
}
