package interfacetest;

public class Aircraft extends Machine implements Voyageable,Roadable,Flyable{

	public Aircraft() {
		super(100);
		
	}

	@Override
	public void voyage() {		
		int v=super.getEnergy();
		super.setEnergy(v-15);
	}

	@Override
	public void fly() {
		int f=super.getEnergy();
		super.setEnergy(f-25);
		
	}

	@Override
	public void run() {
		int r=super.getEnergy();
		super.setEnergy(r-10);
		
	}

}
