package datetime;

import java.util.Calendar;
import java.util.Date;

public final class DateHelper {

	private DateHelper() {
	}
	
	public static DateHelper getInstance() {
		// 自行设计实现过程
		DateHelper datehelper=new DateHelper();
		return datehelper;
	}
	
	public Date toDate( int year , int month , int date , int hourOfDay , int minute ) {
		// 可以将 秒 和 毫秒 部分直接设置为 零
		Calendar calendar=Calendar.getInstance();

        calendar.set(year, month-1, date, hourOfDay, minute);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        
        return calendar.getTime();
	}

	public Date toDate( int year , int month , int date , int hourOfDay , int minute , int second ) {
		// 可以将 毫秒 部分直接设置为 零
		Calendar calendar=Calendar.getInstance();

        calendar.set(year, month-1, date, hourOfDay, minute,second);

        calendar.set(Calendar.MILLISECOND, 0);

        return calendar.getTime();
	}

	public Date toDate( int year , int month , int date ,
	                    int hourOfDay , int minute , int second , int millis ) {
		// 自行设计实现过程
		Calendar calendar=Calendar.getInstance();

        calendar.set(year, month-1, date, hourOfDay, minute,second);
        calendar.set(Calendar.MILLISECOND, millis);

        return calendar.getTime();
	}
	
	public String toString( final Calendar date ) {
		  
		StringBuilder builder = new StringBuilder();
		
		int year = date.get( Calendar.YEAR );
		builder.append( year + "年" );
		
		int month = date.get( Calendar.MONTH ) + 1 ;
		builder.append( month + "月" );
		
		int date1 = date.get( Calendar.DATE );
		if(date1<10) {
			builder.append('0');
		}
		builder.append( date1 + "日  " );
		
		int hours = date.get( Calendar.HOUR_OF_DAY );
		if( hours < 10 ) {
			builder.append( '0' );
		}
		builder.append( hours + ":" );
		
		int minutes = date.get( Calendar.MINUTE );
		if( minutes < 10 ) {
			builder.append( '0' );
		}
		builder.append( minutes + ":" );
		
		int seconds = date.get( Calendar.SECOND );
		if( seconds < 10 ) {
			builder.append( '0' );
		}
		builder.append( seconds + "." );
		
		int millis = date.get( Calendar.MILLISECOND );
		if(millis==0) {
			builder.append("00");
		}
		builder.append( millis );
		
		String s = builder.toString();
		return s ;
	  
	}
	
	public String toString( final Date date ) {
		  
		Calendar c=Calendar.getInstance();
		c.clear();
		c.setTime(date);
		String ss=toString(c);
		return ss;
		}
	
	public int durationOfDays( Date first , Date second ) {
		// 自行设计实现过程
		long day=1000L*60*60*24;
		if(first.before(second)) {
			long x=second.getTime()-first.getTime();
			return (int)(x/day==0 ? -(x/day) : -(x/day)-1);
		}
		if(first.after(second)) {
			long x=first.getTime()-second.getTime();
			return (int)(x/day==0 ? x/day : x/day+1);
		}
		return 0;
	}
}
