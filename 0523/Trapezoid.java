package inheritsecond;

public class Trapezoid extends Shape{
	private double top;
	private double bottom;
	private double height;
	
	public Trapezoid() {
		super("梯形");
	}
		
	public Trapezoid(double top,double bottom,double height) {
		super("梯形");
		this.top=top;
		this.bottom=bottom;
		this.height=height;
	}
	
	public void calculate() {
		super.calculate();
		area=(top+bottom)*height/2;
	}
	
	public void show() {
		System.out.println("梯形的上底为："+top);
		System.out.println("梯形的下底为："+bottom);
		System.out.println("梯形的高为："+height);
		super.show();
	}


}
