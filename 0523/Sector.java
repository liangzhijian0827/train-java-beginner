package inheritsecond;

public class Sector extends Shape{
	private double radius;
	private double arcLength;
	
	
	public Sector() {
		super("扇形");
	}
	
	public Sector(double radius) {
		super("扇形");
		this.radius=radius;
		this.arcLength=10;
	}
	
	public void calculate() {
		super.calculate();
		area=radius*arcLength/2;
	}
	
	public void show() {
		System.out.println("扇形的半径为：" + radius);
		System.out.println("扇形的弧长为：" + arcLength);
		super.show();
		
	}
	

	
}
