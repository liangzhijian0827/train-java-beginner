
public class Person {
    public String name;
    public char gender;
    public int age;
    public boolean married;

    public void marry( Person another ) {
        // 在这里填空，判断自己 ( this ) 跟 另外一个人( another )结婚
            if((this.gender=='男'&&another.gender=='女'&&this.age>=22&&another.age>=20&&this.married==false&&another.married==false)||
                (this.gender=='女'&&another.gender=='男'&&this.age>=20&&another.age>=22&&this.married==false&&another.married==false)){
                    System.out.println(this.name+" 可以和 "+another.name+" 结婚");
                }
            else {
                if((this.married==false&&another.married==true)||(this.married==true&&another.married==false)){
                System.out.println(this.name+" 和 "+another.name+" 婚姻状态不符，不能结婚！");
            }
            if(!(this.gender=='男'&&another.gender=='女'&&this.age>=22&&another.age>=20)||!(this.gender=='女'&&another.gender=='男'&&this.age>=20&&another.age>=22)||
            !(this.gender=='男'&&another.gender=='男'&&this.age>=22&&another.age>=22)||!(this.gender=='女'&&another.gender=='女'&&this.age<20&&another.age<20)){
                System.out.println(this.name+" 和 "+another.name+" 未满足结婚年龄！");
            }
            if((this.gender=='男'&&another.gender=='男')||(this.gender=='女'&&another.gender=='女')) {
                System.out.println(this.name+" 和 "+another.name+" 性别不符不能结婚！");
            }}
    }    
        public static void main(String[] args) {
        Person first=new Person();
        first.name="张三";
        first.gender='男';
        first.age=20;
        first.married=false;

        Person second=new Person();
        second.name="王五";
        second.gender='男';
        second.age=22;
        second.married=false;

        Person third=new Person();
        third.name="小红";
        third.gender='女';
        third.age=20;
        third.married=false;

        Person forth=new Person();
        forth.name="小丽";
        forth.gender='女';
        forth.age=18;
        forth.married=true;

        first.marry(third);
        System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~");
        second.marry(third);
        System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~");
        first.marry(forth);
        System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~");
        first.marry(second);
        System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~");
        third.marry(forth);
        
    }
}