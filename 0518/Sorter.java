
public class Sorter {

    public void traversal( int[] array ){
        if( array != null && array.length > 0 ) {
            for( int i = 0 ; i < array.length ; i++ ){
                System.out.print( array[ i ] );
                System.out.print( ( i < array.length - 1 ) ? " , " : "" );
            }
            System.out.println();
        }
    }

    public int[] sort( int[] array ) {
        int [] result=new int[array.length];
		for(int i=0;i<array.length;i++){
			result[i]=array[i];
        }
        for (int i = 0 ; i < result.length - 1 ; i++) {
            for (int j = 0; j < result.length - 1 - i ; j++) {
                if( result[ j ] > result[ j + 1 ] ) {
                    result[ j ] ^= result[ j + 1 ] ;
                    result[ j + 1 ] ^= result[ j ] ;
                    result[ j ] ^= result[ j + 1 ] ;
                }
            }  
        }
        return  result;
    }

    public static void main(String[] args) {
        Sorter sorter = new Sorter();

        int[] a = { 1 , 100 , -20 , 99 , 1000 , 0 , 30 };
        System.out.print( "原数组: " );
        sorter.traversal( a ); // 原数组: 1 , 100 , -20 , 99 , 1000 , 0 , 30

        int[] x = sorter.sort( a ) ;
        System.out.print( "新数组: " );
        sorter.traversal( x ); // 新数组: -20 , 0 , 1 , 30 , 99 , 100 , 1000

        System.out.print( "原数组: " );
        sorter.traversal( a ); // 原数组: 1 , 100 , -20 , 99 , 1000 , 0 , 30
    }
}