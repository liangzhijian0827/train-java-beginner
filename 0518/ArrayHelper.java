
import java.util.Arrays;
public class ArrayHelper{

    public static boolean equal(int[] first,int[] second){
        if(first.length!=second.length)
            return false;
        else{
            if(Arrays.equals(first,second)){
                return true;
            }
            else{
                return false;
            }  
        }
    }

    public static void main(String[] args) {
        int [] first={1,2,3,4,5};
        int [] second={1,2,4,4,5};
        boolean x=ArrayHelper.equal(first,second );
        System.out.println("数组是否相等："+x);
    }
}