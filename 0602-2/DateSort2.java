package datetime;

import java.time.LocalDate;
import java.util.Random;

public class DateSort2 {

public static void main( String[] args ) {
        
        final Random random = new Random();
    
        final LocalDate[] dates = new LocalDate[ 5 ] ;
        
        // 使用循环对 dates 数组进行初始化
        for( int i = 0 ; i < dates.length ; i++){
            // 随机产生 年份、月份、日期，并以此获得 LocalDate 实例
        	int year=random.nextInt(2020);
        	int month=random.nextInt(12)+1;
        	int day=random.nextInt(31)+1;
            LocalDate date = LocalDate.of(year, month, day);
            dates[ i ] = date ;

        }
        
        // 使用循环输出排序前的日期
        System.out.println("排序前：");
        for(int i=0;i<dates.length;i++) {
        	System.out.println(dates[i]);
        }
        // 使用 LocalDate 类提供的方法，对日期进行比较并排序
        for(int i=0;i<dates.length-1;i++) {
        	for(int n=i+1;n<dates.length;n++) {
        		if(dates[i].isBefore(dates[n])) {
        		LocalDate number=dates[i];
        		dates[i]=dates[n];
        		dates[n]=number;
        		}
        	}
        	
        }
        // 使用循环输出排序后的日期
        System.out.println("排序后：");
        for(int i=0;i<dates.length;i++) {
        	System.out.println(dates[i]);
        }
        
    }
}
