package datetime;

import java.util.Calendar;
import java.util.Date;
import java.util.Random;

public class DateSort1 {

public static void main( String[] args ) {
        
        final Random random = new Random();
    
    		final Calendar calendar = Calendar.getInstance();
        
        final Date[] dates = new Date[ 5 ] ;
        
//        DateFormat df=new SimpleDateFormat("yyyy年MM月dd日");
        
        // 使用循环对 dates 数组进行初始化
        for( int i = 0 ; i < dates.length ; i++){
            // 随机产生 年份、月份、日期，并将其设置到 calendar 对象中
        	int year=random.nextInt(2020);
        	int month=random.nextInt(12)+1;
        	int day=random.nextInt(31)+1;
            calendar.set(year, month, day);
            // 随后使用 calendar 的 getTime 来获取 Date 实例
            Date date = calendar.getTime() ;
            dates[ i ] = date ;
        
        }
        System.out.println("排序前：");
        // 使用循环输出排序前的日期
        for(int i=0;i<dates.length;i++) {
        	System.out.println(dates[i]);
        }
        // 使用 Date 类提供的方法，对日期进行比较并排序
        for(int i=0;i<dates.length-1;i++) {
        	for(int n=i+1;n<dates.length;n++) {
        		if(dates[i].getTime()<dates[n].getTime()) {
        		Date number=dates[i];
        		dates[i]=dates[n];
        		dates[n]=number;
        		}
        	}
        	
        }
        // 使用循环输出排序后的日期
        System.out.println("排序后：");
        for(int i=0;i<dates.length;i++) {
        	System.out.println(dates[i]);
        }
    }
}
